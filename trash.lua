local ut13 = require "utils13"

local sampleBurst = { 0x01, 0x37, 0x38, 0x20, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3B, 0x32, 0x38, 0x30, 0x34, 
						0x30, 0x37, 0x3B, 0x31, 0x31, 0x34,  0x31, 0x3B, 0x05, 0x30, 0x34, 0x36, 0x31, 0x03 }
print( "sample: "..ut13.byteArrayToHexDump( sampleBurst ) )

local testBurst = krypton.getBurst ( 0x38, 0x20, "280407;1141;" )
print( "__test: "..ut13.byteArrayToHexDump( testBurst ) )

local testRBurst = krypton.getResponseBurst ( 0x38, 0x20, "" )
print( "r_test: "..ut13.byteArrayToHexDump( testRBurst ) )

local testBurst2 = krypton.getBurst ( 0x60, 0x4C, "" )
print( "2_test: "..ut13.byteArrayToHexDump( testBurst2 ) )

print( "i2ba: 0x04F1: "..ut13.stringToHexDump( krypton.encodeBcc ( 0x04F1 ) ).." : "..krypton.encodeBcc ( 0x04F1 ) )

