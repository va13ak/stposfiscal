local bit = require "plugin.bit"

local serialConnection

local bnot = bit.bnot
local band, bor, bxor = bit.band, bit.bor, bit.bxor
local lshift, rshift, rol = bit.lshift, bit.rshift, bit.rol

    -- Идентификатор пакета. По идентификатору определяется тип пакета
    REQ = 0x1b -- Информационный пакет, инициирующий начало сессии. Содержит
    -- поле <CMD> и <DATA>
    CAN = 0x18 -- Служебный пакет запроса для отмены  выполнения предыдущего запроса
    -- (предыдущий ответ <ACK2>) в случае если нет выполняемого запроса,
    -- эффекта не дает и ошибки тоже
    NEXT = 0x0C -- Служебный пакет запроса на передачу от ведомого устройства
    -- следующего пакета данных (предыдущий ответ <ACK2>)
    REP = 0x05 -- Служебный пакет запроса на повторную отправку последнего ответа
    -- (предназначен для восстановления данных потерянных при возникновении
    -- ошибок в канале связи)
    ACK0 = 0x06 -- Информационный пакет, подтверждающий прием информационного пакета без
    -- обнаруженных ошибок и содержащий ответ на запрос
    ACK1 = 0x16 -- Служебный пакет, подтверждающий прием информационного пакета
    -- без обнаруженных ошибок и информирующий о том, что запрос обрабатывается,
    -- но ответ в отведенное время не был подготовлен
    ACK2 = 0x1D -- Информационный пакет, подтверждающий прием информационного пакета без
    -- обнаруженных ошибок и содержащий ответ на запрос и указывающий на то,
    -- что у пакета есть еще данные для передачи по текущему запросу
    NAC0 = 0x15 -- Служебный пакет, информирующий о том, что пакет был принят  но отклонен
    -- из-за ошибки при приеме данных (аппаратная ошибка), или ошибка формата
    -- команды. Требуется повторная передача пакета
    NAC1 = 0x04 -- Служебный пакет, информирующий о том, что принятый пакет отклонен в
    -- процессе обработки команды. Например, не выполнены условия для выполнения
    -- этой команды


function DEVutf8ToCp1251 ( str ) -- ФИГНЯ
	print( str:byte( 1, str:len() ) )
	local byteArray = { str:byte( 1, str:len() ) }
	local prevByte = 0
	for i = 1, #byteArray do
		if prevByte > 0 then
--			print( table.indexOf( win_cp1251, band( lshift( prevByte,8 ), byteArray[ i ] ) ) + 127 )
			print( "var1=", 0 + bor( lshift( prevByte, 8 ), byteArray[ i ] ) )
			print( "var2=", 0 + bor( lshift( byteArray[ i ], 8 ), prevByte ) )
			print( prevByte, byteArray[ i ] )
			prevByte = 0
		elseif byteArray[ i ] > 127 then
			prevByte = byteArray[ i ]
			print( "skip-", prevByte )
		else
			print( byteArray[ i ] )
		end
	end
end

function getEmptyArray ( sizeOfArray )
	local array = { }
	for i = 1, sizeOfArray do
		array[ i ] = 0
	end
	return array
end

function stringToByteArray ( str, ... )
	local strLen = str:len()
	local defLen = arg[ 1 ] or strLen
	if ( defLen < 1 ) then return { } end
	local byteArray = { str:byte( 1, ( defLen < strLen ) and defLen or strLen ) }
	if ( defLen > strLen ) then
		for i = #byteArray + 1, defLen do
			byteArray[ i ] = 0
		end
	end
	return byteArray
end

function integerToByteArray ( num, ... )
	if ( arg[ 1 ] and ( arg[ 1 ] < 1 ) ) then return { } end
	local byteArray = { }
	local i = 1
	repeat
		byteArray[ i ] = band( num, 0xFF )
		num = rshift( num, 8 )
		i = i + 1
		if ( arg[ 1 ] and ( i > arg[ 1 ] ) ) then return byteArray end
	until ( num == 0 )
	if ( arg[ 1 ] and ( arg[ 1 ] > #byteArray ) ) then
		for i = #byteArray + 1, arg[ 1 ] do
			byteArray[ i ] = 0
		end
	end
	return byteArray
end

function byteArrayToString ( byteArray )
	return string.char( unpack( byteArray ) )
end

function calculateCrcCcittKermit ( val )
	local crc = 0
	local q = 0
	local c = 0
	local crc = 0x0000
	for i = 1, #val do
        c = val[ i ]
        q = band( bxor( crc, c ), 0x0f )
        crc = bxor( rshift( crc, 4 ), ( q * 0x1081 ) )
        q = band( bxor( crc, rshift( c, 4 ) ), 0x0f )
        crc = bxor( rshift( crc, 4 ), ( q * 0x1081 ) )
	end
	return crc
end


function addCrcToBurst ( burst )
	local crc = calculateCrcCcittKermit( burst )
    local high = math.floor( crc / 256 )
	burst[ #burst + 1 ] = crc - high*256
	burst[ #burst + 1 ] = high
	return burst
end


function getBurst ( id, cmd, data )
	local cmd_array
	if ( type( cmd ) == "string" ) then
		cmd_array = { string.byte( cmd, 1 ), string.byte( cmd, 2 ) }
	else
		cmd_array = cmd
	end

	local burst = table.copy( { id, ( #cmd + #data ), 0 }, cmd_array, data )

	local crc = calculateCrcCcittKermit( burst )
	local high = math.floor( crc / 256 )
	burst[ #burst + 1 ] = crc - high*256
	burst[ #burst + 1 ] = high

	return burst
end


function sendBurst ( burst, waitOnReturn )
	local strBurst = byteArrayToString( burst )

   	print( os.date().." >> "..stringToHexDump( strBurst ) )

	serialConnection:send( strBurst )

	return receiveBurst()
end


function receiveBurst ( )
	local prefix = serialConnection:receive( 3 )
	if not prefix then
    	print( os.date()..": empty prefix"..( prefix or "" ) )
		return nil
	end

	local burstId = prefix:byte( 1 )
	local burstSize = prefix:byte( 2 )

	local body = serialConnection:receive( burstSize + 2 )
	if not body then
    	print( os.date()..": empty body"..prefix..( body or "" ) )
		return nil
	end

    -- Идентификатор пакета. По идентификатору определяется тип пакета
    if ( burstId == REQ ) then 		-- Информационный пакет, инициирующий начало сессии. Содержит
    								-- поле <CMD> и <DATA>
    	-- такое не возвращается
    	print( os.date().." !<< "..stringToHexDump( prefix..body ) )

    elseif ( burstId == CAN ) then 	-- Служебный пакет запроса для отмены  выполнения предыдущего запроса
    								-- (предыдущий ответ <ACK2>) в случае если нет выполняемого запроса,
    								-- эффекта не дает и ошибки тоже
    	-- такое не возвращается
    	print( os.date().." !<< "..stringToHexDump( prefix..body ) )

    elseif ( burstId == NEXT ) then	-- Служебный пакет запроса на передачу от ведомого устройства
    								-- следующего пакета данных (предыдущий ответ <ACK2>)
    	-- такое не возвращается
    	print( os.date().." !<< "..stringToHexDump( prefix..body ) )

    elseif ( burstId == REP ) then 	-- Служебный пакет запроса на повторную отправку последнего ответа
    								-- (предназначен для восстановления данных потерянных при возникновении
    								-- ошибок в канале связи)
    	-- такое не возвращается
    	print( os.date().." !<< "..stringToHexDump( prefix..body ) )

    elseif ( burstId == ACK0 ) then	-- Информационный пакет, подтверждающий прием информационного пакета без
    								-- обнаруженных ошибок и содержащий ответ на запрос
    	print( os.date().." << "..stringToHexDump( prefix..body ) )

    elseif ( burstId == ACK1 ) then	-- Служебный пакет, подтверждающий прием информационного пакета
    								-- без обнаруженных ошибок и информирующий о том, что запрос обрабатывается,
    								-- но ответ в отведенное время не был подготовлен
    	print( os.date().." << "..stringToHexDump( prefix..body ) )

    elseif ( burstId == ACK2 ) then	-- Информационный пакет, подтверждающий прием информационного пакета без
    								-- обнаруженных ошибок и содержащий ответ на запрос и указывающий на то,
    								-- что у пакета есть еще данные для передачи по текущему запросу
    	print( os.date().." << "..stringToHexDump( prefix..body ) )

    elseif ( burstId == NAC0 ) then -- Служебный пакет, информирующий о том, что пакет был принят  но отклонен
    								-- из-за ошибки при приеме данных (аппаратная ошибка), или ошибка формата
    								-- команды. Требуется повторная передача пакета
    	print( os.date().." << "..stringToHexDump( prefix..body ) )

    elseif ( burstId == NAC1 ) then	-- Служебный пакет, информирующий о том, что принятый пакет отклонен в
    								-- процессе обработки команды. Например, не выполнены условия для выполнения
    								-- этой команды
    	print( os.date().." << "..stringToHexDump( prefix..body ) )

    else
    	print( os.date().." !!!<< "..stringToHexDump( prefix..body ) )
   	end
   	return nil
end





function executeXReport ( )
	return sendBurst( getBurst( REQ, "OC", { 0x00, 0x21, 0x30, 0x00, 0x00 } ) )
end

function executeZReport ( )
	return sendBurst( getBurst( REQ, "OC", { 0x01, 0x21, 0x30, 0x00, 0x00 } ) )
end

function cashierRegistration ( )
	return sendBurst( getBurst( REQ, "OB", table.copy( { 0x01 }, getEmptyArray( 4 ) ) ) )
end

function getSerialNumber ( )
	return sendBurst( getBurst( REQ, "04", getEmptyArray( 8 ) ) )
end

function printReceiptCopy ( )
	return sendBurst( getBurst( REQ, "O4" ) )
end

function openCashBox ( )
	return sendBurst( getBurst( REQ, "DO" ) )
end

function cutPaper ( )
	return sendBurst( getBurst( REQ, "PT" ) )
end

--function addPlu( code, taxIndex, dividable, banSale, banCounting,
--                       allowSingleSale, department, price, barcode,
--                       name, quantity )
function addPlu ( params )
	local flag = bor( lshift( band( params.dividable or 0x01, 0x01 ), 3 ), 
					 lshift( band( params.banSale or 0x00, 0x01 ), 4 ),
                	 lshift( band( params.banCounting or 0x01, 0x01 ), 5 ), 
                	 lshift( band( params.allowSingleSale or 0x00, 0x01 ), 6 ) )

	return sendBurst( getBurst( REQ, '30', table.copy( integerToByteArray( params.code or 0, 4 ),
										{ flag, params.department or 1 },
										integerToByteArray( params.price or 0, 4 ),
										integerToByteArray( params.barCode or 0, 8 ),
										stringToByteArray( utf8ToCp1251( params.name ) or "", 48 ),
										integerToByteArray( params.quantity or 0, 4 ) ) ) )
end

--function salePlu(cancelPosition, saleByBarcode, openPrice, quantity, code, price)
function salePlu ( params )
	local saleByBarcode = band( params.saleByBarcode or 0x00, 0x01 )
	local openPrice = band( params.openPrice or 0x01, 0x01 )
	local flag = bor( band( params.cancelPosition or 0x00, 0x01 ), 
					 lshift( saleByBarcode, 1 ),
                	 lshift( openPrice, 2 ) )

	return sendBurst( getBurst( REQ, 'O1', table.copy( { flag }, 
										integerToByteArray( params.quantity or 0, 4 ),
										integerToByteArray( params.code or 0, 4 + saleByBarcode*4 ),
										( openPrice == 0x00 ) and integerToByteArray( params.price or 0, 4 ) or nil ) ) )
end

function openReceipt ( boolRet )
	return getBurst( REQ, "O8", { boolRet or 0x00 } );
end

function cancelReceipt ( )
	return sendBurst( getBurst( REQ, "O4", { } ) )
end

-- function pay(payType, paySum, rrnTransaction)
function pay ( params )
	local rrn = rrnTransaction and 0x00 or -1
	local flag = bor( band( params.payType or 0x00, 0x07 ), 
					 lshift( band( rrn, 0x01 ), 7 ) )

	return sendBurst( getBurst( REQ, 'O2', table.copy( { flag }, 
										integerToByteArray( params.paySum or 0, 4 ),
										( rrn == -1 ) and integerToByteArray( params.rrnTransaction or 0, 4 ) or nil ) ) )
end

-- function inOut(payType, opType, sum, comment, description)
function inOut ( params )
	local flag = bor( band( params.payType or 0x00, 0x07 ), lshift( band( opType or 0x00, 0x01 ), 7 ) )
	local description = utf8ToCp1251( params.description ) or nil
	if ( description ) then
		description:sub( 0, 19 )
		flag = bor( flag, lshift( 0x01, 4 ) )
	end
	local comment = utf8ToCp1251( params.comment ) or nil
	if ( comment ) then
		flag = bor( flag, lshift( 0x01, 5 ) )
		local commLen = comment.len()
		local commLenFinal = 0
		if ( commLen > 224 ) then
			commLenFinal = 224
		elseif ( commLen <= 56 ) then
			commLenFinal = 56
		elseif ( commLen <= 112 ) then
			commLenFinal = 112
		elseif ( commLen <= 224 ) then
			commLenFinal = 224
		end
	end
	return sendBurst( getBurst( REQ, 'O6', table.copy( { flag }, integerToByteArray( sum or 0x00, 4 ), 
										stringToByteArray( comment ), stringToByteArray( description ) ) ) )
end

-- discountSurcharge(opType, haveDN, appRange, opSign, value, opNumber)
function discountSurcharge( params )
	local flag = bor( lshift( band( params.opType or 0x01, 0x01 ) ), 
					 lshift( band( params.haveDN or 0x00, 0x01 ), 1 ),
                	 lshift( band( params.appRange or 0x00, 0x01 ), 6), 
                	 lshift( band( params.opSign or 0x00, 0x01 ), 7) )
	local haveDN = params.haveDN or 0x00
	return sendBurst( getBurst( REQ, 'O3', table.copy( { flag }, integerToByteArray( value or 0x00, 4 ), 
										( haveDN == 1 ) and integerToByteArray( params or 0x00, 8 ) or nil ) ) )
end

function printEmptyReceipt ( )
	openReceipt()
	pay()
end





function stringToHexDump ( str )
    if ( str ) then
        str = string.gsub( str, "(.)",
              function ( c ) return string.format ( "#%02X", string.byte( c ) ) end )
    end
    return str
end

function byteArrayToHexDump ( byteArray )
	return stringToHexDump( byteArrayToString( byteArray ) )
end

function cp1251ToUtf8 ( str )
	if ( str ) then
        str = string.gsub( str, '([^%w" ])',
              function ( c )
              	b = c:byte()
              	if ( b == 168 ) then
           			return string.char( 208, 129 ) -- Ё
           		elseif ( b == 170 ) then
           			return string.char( 208, 132 ) -- Є
           		elseif ( b == 178 ) then
           			return string.char( 208, 134 ) -- І
           		elseif ( b == 175 ) then
           			return string.char( 208, 135 ) -- Ї
           		elseif ( ( b >= 192 ) and ( b <= 239 ) ) then
           			return string.char( 208, b - 48 ) -- 144..191 (А..Яа..п)
           		elseif ( b == 184 ) then
           			return string.char( 209, 145 ) -- ё
           		elseif ( b == 186 ) then
           			return string.char( 209, 148 ) -- є
           		elseif ( b == 179 ) then
           			return string.char( 209, 150 ) -- і
           		elseif ( b == 191 ) then
           			return string.char( 209, 151 ) -- ї
           		elseif ( ( b >= 240 ) and ( b <= 255 ) ) then
           			return string.char( 209, b - 112 ) -- 128..143 (р..я)
           		elseif ( b == 165 ) then
           			return string.char( 210, 144 ) -- Ґ
           		elseif ( b == 180 ) then
           			return string.char( 210, 145 ) -- ґ
              	end
              	return c
              end
              )
	end
	return str
end

function utf8ToCp1251 ( str )
    if ( str ) then
        str = string.gsub( str, '([^%w" ])([^%w" ])',
              function ( c1, c2 )
              	b1 = c1:byte()
              	if ( b1 == 208 ) then
	              	b2 = c2:byte()
              		if ( b2 == 129 ) then
              			return string.char( 168 ) -- Ё
              		elseif ( b2 == 132 ) then
              			return string.char( 170 ) -- Є
              		elseif ( b2 == 134 ) then
              			return string.char( 178 ) -- І
              		elseif ( b2 == 135 ) then
              			return string.char( 175 ) -- Ї
              		elseif ( ( b2 >= 144 ) and ( b2 <= 191 ) ) then
              			return string.char( b2 + 48 ) -- 192..239 (А..Яа..п)
              		end
              	elseif ( b1 == 209 ) then
	              	b2 = c2:byte()
              		if ( b2 == 145 ) then
              			return string.char( 184 ) -- ё
              		elseif ( b2 == 148 ) then
              			return string.char( 186 ) -- є
              		elseif ( b2 == 150 ) then
              			return string.char( 179 ) -- і
              		elseif ( b2 == 151 ) then
              			return string.char( 191 ) -- ї
              		elseif ( ( b2 >= 128 ) and ( b2 <= 143 ) ) then
              			return string.char( b2 + 112 ) -- 240..255 (р..я)
              		end
              	elseif ( b1 == 210 ) then
	              	b2 = c2:byte()
              		if ( b2 == 144 ) then
              			return string.char( 165 ) -- Ґ
              		elseif ( b2 == 145 ) then
              			return string.char( 180 ) -- ґ
              		end
              	end
              	return "?"
              end
              )
    end
    return str
end







local sampleBurst = { 0x1B, 0x0A, 0x00, 0x30, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB7, 0x38 }
-- print( "sample: "..table.concat( sampleBurst, ", " ) )
print( "sample: "..byteArrayToHexDump( sampleBurst ) )

local burst = { 0x1B, 0x0A, 0x00, 0x30, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
burst = addCrcToBurst(burst)
print( "addcrc: "..byteArrayToHexDump( burst ) )

local newBurst = getBurst( REQ, "04", getEmptyArray(8) )
print( "formed: "..byteArrayToHexDump( newBurst ) )


-- local addPluBurst = addPlu({code = 123, name = "good", taxIndex = 1})
-- print( "addplu: "..byteArrayToHexDump( addPluBurst ) )


local str1 = "ABC abc АБВ абв"
-- print( str1..": ", str1:byte(1, str1:len()), "; len=", str1:len())
print( str1 )
print( str1:byte(1, str1:len() ) )
local str2 = utf8ToCp1251(str1)
-- print( str2 )
print( str2:byte(1, str2:len() ) )

local sym1 = "Ґґ Єє Іі Її Ёё"
print ( sym1 )
print( sym1:byte(1, sym1:len() ) )
local sym2 = utf8ToCp1251(sym1)
-- print ( sym2 )
print( sym2:byte(1, sym2:len() ) )

print ( string.char( unpack( { string.byte( "ABCDEFG", 1, 7 ) } ) ) )
--[[
utf8to1251(str1)

print( urlencode(str1) )

print ("fin")
--]]
---[[
local socket = require( "socket" )

-- Connect to the client
-- local client = socket.connect( "10.10.10.79", 23 )
-- serialConnection= socket.connect( "10.10.10.222", 23 )
serialConnection= socket.connect( "localhost", 5555 )
-- Get IP and port from client
local ip, port = serialConnection:getsockname()
-- Print the IP address and port to the terminal
print( "IP Address:", ip )
print( "Port:", port )



-- собачим пятый ком-порт в сеть на порт 23 (терминал)
-- com2tcp.exe --telnet --ignore-dsr COM5 23

-- пробрасываем сетевой порт на COM10
-- com2tcp.exe --telnet --ignore-dsr \\.\CNCA1 10.10.10.79 23

serialConnection:settimeout(10)

getSerialNumber()

cashierRegistration()

serialConnection:close()

--[[ local good = 'Колбаса "Докторская"'
print( good )
local good2 = utf8ToCp1251(good)
print( good2:byte(1, good2:len() ) )
print( byteArrayToHexDump(integerToByteArray(100)) )
--]]

addPlu( { code=100, name='Колбаса "Докторская"', taxIndex=0 } )	-- программируем весовой товар
salePlu( { code=100, quantity=200, price=220 } )	-- продаем весовой товар

addPlu( { code=101, name='Водка "Столичная" 0.5', taxIndex=1, dividable=0 } )	-- программируем штучный товар
salePlu( { code=101, quantity=1000, price=300 } )	-- продаем штучный товар

print ("fin2")
--]]
--[[ load namespace
local socket = require("socket")
-- create a TCP socket and bind it to the local host, at any port
local server = socket.try(socket.bind("*", 0))
-- find out which port the OS chose for us
local ip, port = server:getsockname()
-- print a message informing what's up
print("Please telnet to localhost on port " .. port)
print("After connecting, you have 10s to enter a line to be echoed")
-- loop forever waiting for clients
while 1 do
  -- wait for a conection from any client
  local client = server:accept()
  -- make sure we don't block waiting for this client's line
  client:settimeout(10)
  -- receive the line
  local line, err = client:receive()
  -- if there was no error, send it back to the client
  if not err then client:send(line .. "\n") end
  -- done with client, close the object
  client:close()
end
print("fin")
--]]
--[[
print( "string [1024] --> ["..stringToByteArray("1024").."]" )

local str1024 = "1024"
print( str1024:byte(1 , str1024:len() ) )
print( type(str1024:byte(1 , str1024:len() )) )
local za = {}
local z = table.insert( za, str1024:byte(1 , str1024:len() ) )
print( "formed: "..table.concat( za, ", " ) )
--]]

-- print( "string [1024] --> ["..table.concat( stringToByteArray("1024", ", ")).."]" )
-- local myTextObject = display.newText( "Hello world!", 160, 240, "Arial", 60 )

-- widget.newButton( options )



--[[
local utf8_Cp1251 = { string.char( 208, 160 ) = string:char( 208 ),
					  string.char( 208, 144 ) = string.char( 192 ),
					  string.char( 208, 144 ) = string.char( 193 ),
					  string.char( 208, 144 ) = string.char( 194 ),
					  string.char( 208, 144 ) = string.char( 195 ),
					  string.char( 208, 144 ) = string.char( 196 ),
					  string.char( 208, 144 ) = string.char( 197 ),
					  string.char( 208, 144 ) = string.char( 168 ),
					  string.char( 208, 144 ) = string.char( 198 ),
					  string.char( 208, 144 ) = string.char( 199 ),
					  string.char( 208, 144 ) = string.char( 200 ),
					  string.char( 208, 144 ) = string.char( 201 ),
					  string.char( 208, 144 ) = string.char( 202 ),
					  string.char( 208, 144 ) = string.char( 203 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					  string.char( 208, 144 ) = string.char( 204 ),
					}

--]]
--[[	

function cp1251_to_utf8 ($txt)  { 
    $in_arr = array ( 
        chr(208), chr(192), chr(193), chr(194), 
        chr(195), chr(196), chr(197), chr(168), 
        chr(198), chr(199), chr(200), chr(201), 
        chr(202), chr(203), chr(204), chr(205), 
        chr(206), chr(207), chr(209), chr(210), 
        chr(211), chr(212), chr(213), chr(214), 
        chr(215), chr(216), chr(217), chr(218), 
        chr(219), chr(220), chr(221), chr(222), 
        chr(223), chr(224), chr(225), chr(226), 
        chr(227), chr(228), chr(229), chr(184), 
        chr(230), chr(231), chr(232), chr(233), 
        chr(234), chr(235), chr(236), chr(237), 
        chr(238), chr(239), chr(240), chr(241), 
        chr(242), chr(243), chr(244), chr(245), 
        chr(246), chr(247), chr(248), chr(249), 
        chr(250), chr(251), chr(252), chr(253), 
        chr(254), chr(255) 
    );    
  
    $out_arr = array ( 
        chr(208).chr(160), chr(208).chr(144), chr(208).chr(145), 
        chr(208).chr(146), chr(208).chr(147), chr(208).chr(148), 
        chr(208).chr(149), chr(208).chr(129), chr(208).chr(150), 
        chr(208).chr(151), chr(208).chr(152), chr(208).chr(153), 
        chr(208).chr(154), chr(208).chr(155), chr(208).chr(156), 
        chr(208).chr(157), chr(208).chr(158), chr(208).chr(159), 
        chr(208).chr(161), chr(208).chr(162), chr(208).chr(163), 
        chr(208).chr(164), chr(208).chr(165), chr(208).chr(166), 
        chr(208).chr(167), chr(208).chr(168), chr(208).chr(169), 
        chr(208).chr(170), chr(208).chr(171), chr(208).chr(172), 
        chr(208).chr(173), chr(208).chr(174), chr(208).chr(175), 
        chr(208).chr(176), chr(208).chr(177), chr(208).chr(178), 
        chr(208).chr(179), chr(208).chr(180), chr(208).chr(181), 
        chr(209).chr(145), chr(208).chr(182), chr(208).chr(183), 
        chr(208).chr(184), chr(208).chr(185), chr(208).chr(186), 
        chr(208).chr(187), chr(208).chr(188), chr(208).chr(189), 
        chr(208).chr(190), chr(208).chr(191), chr(209).chr(128), 
        chr(209).chr(129), chr(209).chr(130), chr(209).chr(131), 
        chr(209).chr(132), chr(209).chr(133), chr(209).chr(134), 
        chr(209).chr(135), chr(209).chr(136), chr(209).chr(137), 
        chr(209).chr(138), chr(209).chr(139), chr(209).chr(140), 
        chr(209).chr(141), chr(209).chr(142), chr(209).chr(143) 
    );    
  
    $txt = str_replace($in_arr,$out_arr,$txt); 
    return $txt; 
}



Copied from: http://articles.org.ru/cn/showdetail.php?cid=8529
--]]


-- about encodings/transcoding http://7maze.ru/node/29

--[[
local win_cp1251 =
  { 0x0402, 0x0403, 0x201A, 0x0453, 0x201E, 0x2026, 0x2020, 0x2021,
	0x20AC, 0x2030, 0x0409, 0x2039, 0x040A, 0x040C, 0x040B, 0x040F,
	0x0452, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
	0xFFFD, 0x2122, 0x0459, 0x203A, 0x045A, 0x045C, 0x045B, 0x045F,
	0x00A0, 0x040E, 0x045E, 0x0408, 0x00A4, 0x0490, 0x00A6, 0x00A7,
	0x0401, 0x00A9, 0x0404, 0x00AB, 0x00AC, 0x00AD, 0x00AE, 0x0407,
	0x00B0, 0x00B1, 0x0406, 0x0456, 0x0491, 0x00B5, 0x00B6, 0x00B7,
	0x0451, 0x2116, 0x0454, 0x00BB, 0x0458, 0x0405, 0x0455, 0x0457,
	0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
	0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
	0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
	0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
	0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
	0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
	0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
	0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F}
--]]