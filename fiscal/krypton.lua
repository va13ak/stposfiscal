local bit = require "plugin.bit"

local ut13 = require "fiscal.utils13"
local stringToByteArray = ut13.stringToByteArray
local Utf8ToAnsi = ut13.Utf8ToAnsi
local byteArrayToHexDump = ut13.byteArrayToHexDump
local stringToHexDump = ut13.stringToHexDump

local serialConnection

local bnot = bit.bnot
local band, bor, bxor = bit.band, bit.bor, bit.bxor
local lshift, rshift, rol = bit.lshift, bit.rshift, bit.rol

local curSeq = 0x20;	-- last created burst id
local lastSeq;	-- last sended burst id


	NAK = 0x15	-- Посылается фискальным регистратором, если контрольная сумма принятого сообщения неверна. 
				-- После получения NAK необходимо повторить сообщение, инкрементировав его порядковый номер.
	SYN = 0x16	-- Иногда отработка команды может занимать значительное время. В таком случае сообщение SYN 
				-- посылается фискальным регистратором каждые 60ms, пока не будет готово ответное сообщение.
	
	-- 2.4	Формат сообщения
	------------------------
	--	а) от PC к регистратору 
	--		< SOH ><len><seq><cmd><pwd><data><ENQ><bcc><ETX>
	--	б) от фискального регистратора к PC (ответное сообщение).
	--		< SOH ><len><seq><cmd><error code><data><EOT><status><ENQ><bcc><ETX>

	SOH = 0x01	-- преамбула (открывающая скобка сообщения)
				-- длина:  1 байт
				-- значение: 01h
	-- <len> - длина сообщения . Равняется количеству байтов, находящихся между <01> и  <bcc> плюс 20h.
				-- длина:  1 байт
				-- значение: 20h – FFh
	-- <seq> - порядковый номер сообщения.
				-- длина:  1 байт
				-- значение: 20h - 7Fh
				-- Фискальный регистратор записывает полученный  <seq> в ответное сообщение.
	-- <pwd> - Пароль 
				-- длина:  6 байт
				-- значение:  20h – FFh
				-- Регистратор осуществляет проверку пароля в зависимости от кода команды, переданного в 
				--	сообщении. Существует следующие виды паролей:
				--	Пароль отчетов 
				--	Пароль программирования
				--	Пароли операторов (торговые пароли)
				--	Пароль Администратора
				-- Некоторые команды не требуют передачи пароля, поэтому пароль, переданный в сообщении с такими
				--	командами регистратором не проверяется. В сервисном режиме регистратор не проверяет пароль 
				--	Администратора. Список паролей приведен в приложении А 
				-- после него идет символ ‘;’ (3Bh).
	-- <cmd> - код  команды.
				-- длина:   1 байт
				-- значение:  20h - FFh
				-- Регистратор записывает полученный <cmd> в ответное сообщение.
	-- <data> - данные.
				-- длина: 0 – 120 
				-- значение: 20h – FFh
				-- Формат и длина поля <data> зависит от команды.
				-- Поле <data> может быть нулевым (не содержит ни одного байта) либо состоять из  одного или 
				--	нескольких аргументов. Каждый аргумент завершается символом ‘;’ (3Bh). Типы аргументов 
				--	см. таблицу 2.
				-- При ошибке возвращается пакетное сообщение с нулевой длиной поля данных и соответствующим кодом ошибки.
	EOT = 0x04	-- Разделитель.
				-- длина:    1 байт
				-- значение:   04h
	-- <status> - поле с текущим состоянием фискального регистратора. 
				-- длина:    6 байтов
				-- значение:   80h – FFh (см. таблицу 1)
	ENQ = 0x05	-- постамбула (закрывающая скобка сообщения)
				-- длина:    1 байт
				-- значение:    05h
	-- <bcc> - контрольная сумма – это два младших байта суммы байтов сообщения (суммированию подлежит часть сообщения, 
				--	начиная с первого байта за преамбулой ( SOH ) вплоть до постамбулы (ENQ), включая ее).
				-- Диапазон значений: 0000h-FFFFh. 
				-- длина:    4 байта
				-- каждая цифра контрольной суммы передается в виде символа, код которого равен значению цифры плюс 30h. 
				-- Например, сумма 1AE3h передается как последовательность символов с кодами 31h, 3Ah, 3Eh, 33h.
	ETX = 0x03	-- терминатор (завершающий байт)
				-- длина:    1  байт
				-- значение:    03h

	-- Пример команды: код 0х20 пароль «000000» данные «280407;1141;»
	--		01 37 38 20 30 30 30 30 30 30 3B 32 38 30 34 30 37 3B 31 31 34 31 3B 05 30 34 36 31 03  


	-- 2.5 Проверка принятого пакета
	--------------------------------
	-- Проверка правильности пакета осуществляется:
	--	1.	Непосредственно при приеме данных проверяется правильность формата сообщения
	--	2.	После приема всего пакета выполняется проверка контрольной суммы
	--	3.	Сравнение номера пакета с предыдущим (если неравен обновляем внутренний счетчик)
	--	4.	поиск команды в списке команд
	--	5.	проверка полей данных на формат

	--	Обработка ошибок
	--	Регистратор сообщает об ошибках только по пунктам 2-5, в других случаях
	--	1.	При возникновении ошибки в формате пакета регистратор переходит в ожидание начала пакета
	--	2.	принятие байта SOH в теле пакета  начинает принятие сообщения с начала. Предыдущий пакет игнорируется.


--local documentStates = { 0x00 = "closed", 0x01 = "hasTitle", 0x02 = "hasSale", 0x03 = "has"}

local errorCodesDescription = {	
	[0x0000] = "Без ошибок",
	[0x0001] = "Неопределенная ошибка оборудования",
	[0x0101] = "Ошибка принтера",
	[0x0201] = "Ошибка RAM",
	[0x0301] = "Ошибка Контрольная сумма памяти программ",
	[0x0401] = "Ошибка flash памяти",
	[0x0501] = "Ошибка Дисплея",
	[0x0601] = "Ошибка часов, необходимо скорректировать время",
	[0x0701] = "Ошибка возникла вследствие понижения питания",
	[0x0801] = "SIM: отказ оборудования",
	[0x0901] = "MMC: отказ оборудования",
	
	[0x0002] = "Нет такой команды с таким кодом",
	[0x0102] = "Данная функция не поддерживается",
	[0x0202] = "В сообщении нет данных",
	[0x0302] = "Переполнение буфера приемника/передатчика",
	
	[0x0003] = "Формат поля [0xNN03 неверен, где NN -Порядковый номер неверного поля",
	
	[0x0004] = "Значение поля [0xNN04 неверен, где NN -Порядковый номер неверного поля",
	[0x0104] = "",
	[0x0204] = "",
	[0x0304] = "",
	[0x0404] = "",
	[0x0504] = "",
	[0x0604] = "",
	[0x0704] = "",
	
	[0x0005] = "Нет свободного места для записи в ФП",
	[0x0105] = "Ошибка записи в ФП",
	[0x0205] = "Заводской номер не установлен",
	[0x0305] = "Дата последней записи меньше чем та, что пытаемся установить",
	[0x0405] = "Нельзя выходить за пределы суток",
	[0x0505] = "Ошибка фискальной памяти",
	[0x0605] = "Фискальная память исчерпана",
	[0x0705] = "Данная команда выполняется только в фискальном режиме",
	[0x0805] = "дата и время не были установлены с момента последнего аварийного обнуления ОЗУ",
	[0x0905] = "С начала смены прошло более 24х часов",
	[0x0A05] = "Необходимо скорректировать время",
	[0x0B05] = "Ошибка в формате таблицы",
	[0x0C05] = "Ошибка в формате даты",
	[0x0D05] = "Ошибка в формате времени",
	[0x0E05] = "Время что пытаемся установить меньше текущего",
	[0x0F05] = "Есть не отправленные эквайру документы в течение 72 часов или более",
	
	[0x0006] = "Неверный пароль доступа",
	
	[0x0007] = "Неверный режим аппарата (перемычка)",
	[0x0107] = "В данном состоянии смены невыполнима",
	[0x0207] = "Необходимо выгрузить контрольную ленту (бумажную/подписанную)",
	[0x0307] = "Аппарат находится в автономном режиме",
	[0x0407] = "Смена закрыта но необнулена",
	[0x0507] = "Персонализация не выполнена",
	[0x0607] = "Не все данные переданы эквайру перед сменой налоговых номеров",
	[0x0707] = "Версия не активирована",
	[0x0907] = "Неверный ключ активации версии",
	[0x0A07] = "Удаленный сервис не активирован",
	
	[0x0008] = "Переполнение математики",
	
	[0x0009] = "Не очищено",
	[0x0109] = "Чтение/запись по неинициализированному указателю",
	[0x0209] = "Адрес/параметр за пределами зоны",
	[0x0309] = "Выделен недостаточный буфер",
	[0x0409] = "Ошибка данных",
	
	[0x000A] = "Недостаточно свободного места для выполнения команды",
	[0x010A] = "Длина записи больше максимума (>255)",
	[0x020A] = "Артикул/Кассир/.. с данным кодом не найден",
	[0x030A] = "Индекс за пределами базы",
	[0x040A] = "Артикул с данным кодом существует",
	[0x050A] = "Налог запрещен",
	[0x060A] = "Продаж по налоговой группе не было",
	[0x070A] = "Использование такого типа налога запрещено",
	
	[0x000B] = "Неверное состояние документа",
	[0x010B] = "Недостаточно свободного места для выполнения команды",
	[0x020B] = "Неизвестный тип записи продажи",
	[0x030B] = "Аннуляция: не может начинаться с данной операции",
	[0x040B] = "Аннуляция: данная операция в чеке не найдена",
	[0x050B] = "Аннуляция: последовательность неполная",
	[0x060B] = "Аннулировать нечего",
	[0x070B] = "Копия заданных чеков недоступна",
	[0x080B] = "Недостаточно наличности для выполнения операции",
	[0x090B] = "Данный вид оплаты в этом чеке запрещен",
	[0x0A0B] = "Сдача с данного вида оплаты запрещена",
	[0x0B0B] = "Значение скидки вышло за пределы",
	[0x0C0B] = "Переполнение итога по чеку",
	[0x0D0B] = "Переполнение по оплатам",
	[0x0E0B] = "Вышли за пределы буфера",
	[0x0F0B] = "Продажа: ошибка в количестве",
	[0x100B] = "Продажа: ошибка в цене",
	[0x110B] = "Аннуляция: удаляемая последовательность заблокирована",
	[0x120B] = "Продажа: достигнуто максимальное количество позиций в чеке (комментарии считаются как позиции)",
	
	[0x00C0] = "TMPBuff: Не соответствует заголовок",
	[0x01C0] = "TMPBuff: данные не совпали с ранее сохраненными",
	[0x02C0] = "TMPBuff: за пределами буфера",
	
	[0x00D0] = "SIM: неправильный порядок вычисления хэша",
	[0x01D0] = "SIM: подпись не совпадает",
	[0x02D0] = "SIM: текущий открытый ключ не соответствует сохраненному в ФП",
	[0x03D0] = "SIM: Ошибка закрытого ключа",
	[0x04D0] = "SIM: ID_DEV не установлен",
	[0x05D0] = "SIM: Неверное состояние карты",
	[0x06D0] = "SIM: Ошибка подписывания",
	[0x07D0] = "SIM: Ошибка формата XML",
	[0x08D0] = "SIM: Ошибка версии ключа",
	[0x09D0] = "SIM: Документ уничтожен",
	[0x0AD0] = "SIM: Документ изменен",
	[0x0BD0] = "SIM: Неверный размер документа",
	[0x0CD0] = "SIM: Копия документа временно не доступна. Идет передача данных.",
	
	[0x00D1] = "MMC: карточку необходимо форматировать",
	[0x01D1] = "MMC:Недостаточно свободного места для выполнения команды",
	[0x02D1] = "MMC: Ошибка создания файла, файл с таким именем уже существует",
	[0x03D1] = "MMC: Ошибка чтения файла, запрошенный размер на чтение больше фактического размера файла",
	[0x04D1] = "MMC: Попытка произвести операции с открытым файлом",
	[0x05D1] = "MMC: ошибка открытия файла - не найден файл или директорию",
	[0x06D1] = "MMC: AccessMode",
	[0x07D1] = "MMC: rErrSD_PathLength",
	[0x08D1] = "MMC: Ошибка открытия/создания файла",
	[0x09D1] = "MMC: Ошибка записи файла",
	
	[0x00F0] = "ФСтрока: Параметры",
	[0x01F0] = "ФСтрока: Тип данных",
	[0x02F0] = "ФСтрока: Выбор шрифта",
	[0x03F0] = "ФСтрока: Выравнивание",
	[0x04F0] = "ФСтрока: Позиции в строке",
	[0x05F0] = "DBF: Формат файла неверен",
	[0x06F0] = "DBF: Количество полей больше максимального",
	[0x07F0] = "DBF: Обращение к несуществующей записи",
	[0x08F0] = "DBF: Недопустимый тип поля",
	[0x09F0] = "DBF: Неверное значение в поле",
	[0x0AF0] = "DBF: Значение для поиска не задано",
	[0x0BF0] = "DBF: do not open",
	
	[0x00F1] = "Строка ввода: Величина за пределами",
	[0x01F1] = "Строка ввода: Нельзя изменять",
	[0x02F1] = "Отменено пользователем"
};


function calculateBcc ( val )
	local bcc = 0;
	for i = 1, #val do
		bcc = bcc + val[ i ]
	end
	return band(bcc, 0xffff);
end

function encodeBcc ( bcc )
	local bccEncoded = ""
	local _bcc = bcc
	for i = 1, 4 do
		bccEncoded = string.char( 0x30+band( _bcc, 0x0F ) )..bccEncoded;
		_bcc = rshift( _bcc, 4);
	end
	return bccEncoded;
end


local function getBurst ( seq, cmd, _data, password )
	local data = Utf8ToAnsi( _data or "" )
	local burst = table.copy( { 3 + 7 + #data + 1 + 0x20, seq or getNextSeq(), cmd },
								stringToByteArray( string.format("%06d", password or 0)..";" ), 
								stringToByteArray( data ), 
								{ ENQ } )
	
	
	local encodedBcc = encodeBcc( calculateBcc( burst ) )
	for i = 1, #encodedBcc do
		table.insert( burst, #burst+1, encodedBcc:byte(i) );
	end


	table.insert( burst, 1, SOH );

	table.insert( burst, #burst+1, ETX );

	return burst
end


function getResponseBurst ( seq, cmd, data, errorCode, status )	-- generating fake response burst
	local burst = table.copy( { 3 + 5 + #data + 1 + 6 + 1 + 0x20, seq, cmd },
								stringToByteArray( string.format("%04d", errorCode or 0)..";" ),
								stringToByteArray( Utf8ToAnsi( data ) ), 
								{ EOT },
								status or { 0x80, 0x80, 0x80, 0x80, 0x80, 0x80 }, 
								{ ENQ } )

	local byteArrayBcc = { string.format("%04X", calculateBcc( burst )):byte( 1, 4 ) }
	for i = 1, #byteArrayBcc do
		table.insert( burst, #burst+1, byteArrayBcc[ i ] );
	end

	table.insert( burst, 1, SOH );

	table.insert( burst, #burst+1, ETX );

	return burst
end

local function readAnswer(client)
	local header, _, _ = string.byte(client:receive(1) or "");
	if (header ~= SOH) then
		print( ">>> header: "..(header or "nil") );
		return nil, nil, nil;
	end;

	local sLength, _, _ = client:receive(1) or "";
	local length = string.byte( sLength );
	if (length == 0) then
		print( ">>> length(raw): "..(length or "nil") );
		return nil, nil, nil;
	end;

	length = length - 0x20;
	--print( "<<< length: "..length );

	local bodyraw, _, _ = client:receive(length+4);

	bodyraw = sLength..bodyraw;

	local body = stringToByteArray( bodyraw );
	--print("seq: "..bodyraw:byte(2))
	--print("cmd: "..bodyraw:byte(3))
	local data = bodyraw:sub(4,length-1-6-1);
	--print( "data: "..data);
	local values = {}
	for word in data:gmatch('([^;]+)') do
    	--print(word);
    	table.insert( values, word );
	end

	print( unpack(values) )

	local sBody = string.sub( bodyraw, 1, length );
	print( ">>> sBody: "..sBody);

	local aBody = stringToByteArray( sBody );
	--print( "<<< aBody: "..byteArrayToHexDump( aBody ) );
	local bcc = calculateBcc( aBody );
	local sBcc = encodeBcc ( bcc );
	--print( "calced BCC: "..sBcc );
	--print( "calced BCC: "..byteArrayToHexDump({ sBcc:byte( 1, 4 ) }) );
	--print( bodyraw:sub(#bodyraw-4,#bodyraw-1) );
	if (sBcc == bodyraw:sub(#bodyraw-4,#bodyraw-1) ) then
		print( "!!! BCC check is OK ("..byteArrayToHexDump({ sBcc:byte( 1, 4 ) }) )

		curSeq = bodyraw:byte(2); -- !!!!!!!!!!!!!!!!!!!!!!!!!!
	else
		print( "!!! BCC check is FAILED: "..byteArrayToHexDump({ sBcc:byte( 1, 4 ) }).." != "..stringToHexDump(bodyraw:sub(#bodyraw-4,#bodyraw-1)) )
	end

	local statusBytes = bodyraw:sub(#bodyraw-11,#bodyraw-6)
	print( ">>> status: "..stringToHexDump(statusBytes) )


	print ( ">>> body(raw): "..bodyraw );
	print ( ">>> body: "..byteArrayToHexDump ( body ) );

	return #body+1, values, stringToByteArray( statusBytes )
end


function unpackBurst( data, readNext )
	local status = true;

	if (not data[ 1 ] == SOH) then return false; end;

	local size = 0;

	for pos = 1, #data do
		curByte = data[ pos ];

	end

	while readNext do

	end
end


function decodeStatus( ... )
	-- body
end


local function getXReportBurst( seq, password )
	return getBurst( seq, 0xA1, "1;", password)
end

local function getZReportBurst( seq, password )
	return getBurst( seq, 0xA1, "0;", password )
end

function getOpenFiscalCheckBurst( seq, type, password )
	return getBurst(seq, 0x63, "01;1;"..tostring(type or 0)..";", password )
end

function getCloseFiscalCheckBurst( seq, info, password )
	return getBurst(seq, 0x65, ""..tostring( band( info or 0, 0xFF ))..";", password )
end

function getPayCheckBurst( seq, type, sum, password )
	--return getBurst(seq, 0x67, ""..tostring( band( type or 0, 0xFF ))..";"..tostring( band( sum or 0, 0xFFFFFFFF ))..";", password )
	return getBurst(seq, 0x67, ""..tostring( band( type or 0, 0xFF ))..";"
									..tostring( sum or 0 )..";", password )
end

function getSaleBurst( seq, code, count, price, password )
	return getBurst(seq, 0x67, ""..tostring( code )..";"
									..tostring( count or 1 )..";"
									..tostring( price or 0 )..";", password )
end

function getDiscountBurst( seq, sum, password )
	return getBurst(seq, 0x69, ""..tostring( sum or 0 )..";", password )
end

function getVoidCheckBurst( seq, oper, password )
	return getBurst(seq, 0x6B, ""..tostring( band( oper or 0, 0xFF ))..";", password )
end

local function getOpenTextDocumentBurst( seq, password )
	return getBurst( seq, 0x60, "", password )
end

local function getPrintTextBurst( seq, text, password )
	--local encodedText = Utf8ToAnsi( text );	-- getBurst will code the string
	local encodedText = text;
	if (#encodedText > 75) then
		encodedText = encodedText:sub(1, 75);
	end
	return getBurst( seq, 0x61, ""..encodedText, password )
end

local function getCloseCheckBurst( seq, password )
	return getBurst(seq, 0x65, "0;", password )
end

local function responseWithError( data )
	if (not data) then
		return nil;
	end;
	local sErrorCode = data[1] or 0;
	local errorCode = tonumber("0x"..sErrorCode);
	print("error code: "..(sErrorCode or "")..": ", errorCodesDescription[errorCode] );
	if (errorCode ~= 0) then
		return errorCode;
	end;
	return nil;
end

local function printTextDocument ( client, text )
				client:settimeout(1);

				local xBurst
				local _s, _v, _st
				local errorCode

				print( "::!!!!!:: try to open text document" )
				xBurst = getOpenTextDocumentBurst( );
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print (  string.char( unpack( xBurst ) )  )
				print( ">>> "..byteArrayToHexDump( xBurst ) );
				_s, _v, _st = readAnswer( client )
				print( _s, _v, _st );
				errorCode = responseWithError (_v);
				if (errorCode) then
					print("status bytes: "..byteArrayToHexDump(_st) );
					if (errorCode == 0x000B) then
						local checkStatus = band(_st[3] or 0, 0x0f);
						if (checkStatus == 0x05) then
							-- текстовый чек уже открыт печатаем чего надо
						else
							return;
						end;
					else
						return;
					end;
				end;

				print( "::!!!!!:: try to print text" )
				xBurst = getPrintTextBurst( nil, text..";" );
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print (  string.char( unpack( xBurst ) )  )
				print( ">>> "..byteArrayToHexDump( xBurst ) );
				_s, _v, _st = readAnswer( client )
				print( _s, _v, _st );
				errorCode = responseWithError (_v);
				if (errorCode) then
					print("status bytes: "..byteArrayToHexDump(_st) );
				end;

				print( "::!!!!!:: try to close text document" )
				xBurst = getCloseCheckBurst( );
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print (  string.char( unpack( xBurst ) )  )
				print( ">>> "..byteArrayToHexDump( xBurst ) );
				_s, _v, _st = readAnswer( client )
				print( _s, _v, _st );
				errorCode = responseWithError (_v);
				if (errorCode) then
					print("status bytes: "..byteArrayToHexDump(_st) );
					return;
				end;
end

function getSeq( ... )
	return math.random( 0x20, 0x7F );
end

function getNextSeq( ... )
	if (curSeq < 0x20) and (curSeq > 0x7F) then
		curSeq = 0x20;
	else
		curSeq = curSeq + 1;
	end
	return curSeq;
end


return {
	getBurst = getBurst,
	getResponseBurst = getResponseBurst,
	calculateBcc = calculateBcc,
	encodeBcc = encodeBcc,
	getXReportBurst = getXReportBurst,
	getZReportBurst = getZReportBurst,
	printTextDocument = printTextDocument,
	readAnswer = readAnswer
}