local widget = require( "widget" )

local newLinePos = 0

local scrollView

local topFirstButtonRow

local buttonHeight

local colButtonIdent
local rowButtonIdent

local colButtonsNumber
local rowButtonsNumber

local buttonHandler

local buttons = {}

local function sVprint( ... )

	local stringForPrint = ""

	for i, v in ipairs(arg) do
		-- stringForPrint = stringForPrint .. tostring( v ) .. "\t"
		stringForPrint = stringForPrint .. tostring( v ) .. " "
	end
		
	stringForPrint = stringForPrint .. "\n"

	local stringForPrintObject = display.newText( stringForPrint, display.contentCenterX, 0, 300, 0, native.systemFont, 14)
	stringForPrintObject:setFillColor( 0 ) 
	stringForPrintObject.anchorY = 0.0		-- Top
	stringForPrintObject.y = newLinePos
	newLinePos = stringForPrintObject.y + stringForPrintObject.contentHeight + 10

	scrollView:insert( stringForPrintObject )

	scrollView:scrollTo( "bottom", { time = 0, onComplete = onScrollComplete } )
end


local function addButton(row, col, buttonId, buttonLabel)
	local buttonWidth = (display.contentWidth - colButtonIdent * (colButtonsNumber - 1)) / colButtonsNumber
	buttons[buttonId] = widget.newButton {
		id = buttonId,
		label = buttonLabel,


		left = 0 + (buttonWidth + colButtonIdent) * (col - 1),
		width = buttonWidth,

		top = topFirstButtonRow + (buttonHeight + rowButtonIdent) * (row - 1),
		height = buttonHeight,

		onEvent = buttonHandler,

	    emboss = false,
	    --properties for a rounded rectangle button...
	    shape = "roundedRect",
	    cornerRadius = 1,
	    --fillColor = { default = { 1, 0, 0, 1 }, over = { 1, 0.1, 0.7, 0.4 } },
	    --strokeColor = { default = { 1, 0.4, 0, 1 }, over = { 0.8, 0.8, 1, 1 } },
	    strokeWidth = 4
	}
	return buttons[buttonId];
end






colButtonsNumber = 2
rowButtonsNumber = 4

colButtonIdent = 4
rowButtonIdent = 4

buttonHeight = 25

topFirstButtonRow = display.contentHeight + 1
			- ( (rowButtonsNumber - 1) * rowButtonIdent + rowButtonsNumber * buttonHeight )

-- Create a ScrollView
scrollView = widget.newScrollView
{
	left = 0,
	top = 0,
	width = display.contentWidth,
	height = topFirstButtonRow - 5,
	-- bottomPadding = 50,
	id = "onBottom",
	horizontalScrollDisabled = true,
	verticalScrollDisabled = false,
	listener = scrollListener,
}

local function setColButtonsNumber( _colButtonsNumber )
	colButtonsNumber = _colButtonsNumber;
end

local function setButtonHandler( _buttonHandler )
	buttonHandler = _buttonHandler;
end

local function getButton( name )
	return buttons[name];
end

local function enableButton( name, enabled )
	local button = getButton(name);
	if (button == nil) then
		return;
	end;
	button:setEnabled(enabled);
	local dR, dG, dB, dA = display.getDefault( "fillColor" );
	if (enabled) then
		button:setFillColor( dR, dG, dB, dA );
	else
		button:setFillColor( dR, dG, dB, dA - 70 );
	end;
end

return {
	addButton = addButton,
	setColButtonsNumber = setColButtonsNumber,
	setButtonHandler = setButtonHandler,
	getButton = getButton,
	enableButton = enableButton,
	sVprint = sVprint
}