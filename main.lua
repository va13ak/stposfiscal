-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
local krypton = require "fiscal.krypton"
local ut13 = require "fiscal.utils13"
local btn13 = require "buttons13"

local client
local clientConnected = false

do
	local old = print
	print = function ( ... )
		btn13.sVprint ( os.date( "%H:%M:%S", os.time()), ": ", ... )
		return old( os.date( "%H:%M:%S", os.time()), ": ", ... )
	end
end

local buttonHandler = function( event )
	-- print ( "id = " .. event.target.id .. ", phase = " .. event.phase )
	if event.phase == "ended" then
		print ( "--- action: " .. event.target.id .. " ---")
		if event.target.id == "getSerial" then
			if clientConnected == true then
				client:setoption("tcp-nodelay", true);
				server:setoption("tcp-nodelay", true);
				local xBurst = krypton.getBurst( nil, 0x4c, "");
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print (  string.char( unpack( xBurst ) )  )
				print( "<<< "..ut13.byteArrayToHexDump( xBurst ) );
				server:settimeout(1);
				client:settimeout(1);
				print( krypton.readAnswer( client ) );

				local xBurst = krypton.getBurst( nil, 0x42, "");
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print (  string.char( unpack( xBurst ) )  )
				print( "<<< "..ut13.byteArrayToHexDump( xBurst ) );
				server:settimeout(1);
				client:settimeout(1);
				print( krypton.readAnswer( client ) );
			end
		end
		if event.target.id == "regCashier" then
		end
		if event.target.id == "testCheque" then
			krypton.printTextDocument ( client, "Привет, принтер! Hello, printa!" );
			--krypton.printTextDocument ( client, "Hello printa" );
		end
		if event.target.id == "testOutput" then
		end
		if event.target.id == "xReport" then
			if clientConnected == true then
				local xBurst = krypton.getXReportBurst();
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print( ">>> "..ut13.byteArrayToHexDump( xBurst ) );
				server:settimeout(1);
				client:settimeout(1);
				print( krypton.readAnswer( client ) );
			end
		end
		if event.target.id == "zReport" then
			if clientConnected == true then
				local xBurst = krypton.getZReportBurst();
				print( client:send( string.char( unpack( xBurst ) ) ) );
				print( "<<< "..ut13.byteArrayToHexDump( xBurst ) );
				server:settimeout(1);
				client:settimeout(1);
				print( krypton.readAnswer( client ) );
			end
		end
		if event.target.id == "emptyReceipt" then
		end
		if event.target.id == "connect" then
			if client ~= nil then
				client:close();
				client = nil;
				clientConnected = false;
				btn13.getButton("connect"):setLabel( "not connected");
				btn13.enableButton("connect", false );
			end
		end
	end
end


btn13.setButtonHandler( buttonHandler )
btn13.setColButtonsNumber( 1 )
btn13.addButton (1, 1, "testOutput", "test output")
btn13.setColButtonsNumber( 3 )
btn13.addButton (2, 1, "getSerial", "get serial")
btn13.addButton (2, 2, "regCashier", "reg cashier")
btn13.addButton (2, 3, "testCheque", "test receipt")

btn13.addButton (3, 1, "emptyReceipt", "empty receipt")
btn13.addButton (3, 2, "xReport", "x-report")
btn13.addButton (3, 3, "zReport", "z-report")
btn13.setColButtonsNumber( 1 )
btn13.addButton (4, 1, "connect", "not connected")
btn13.enableButton( "connect", false );



-- socket server articles
-- https://coronalabs.com/blog/2014/09/23/tutorial-local-multiplayer-with-udptcp/
-- https://books.google.com.ua/books?id=XvU7aH0k8Z4C&pg=PA216&lpg=PA216&dq=corona+lua+socket+server&source=bl&ots=gzV4IIXJ97&sig=s0L1xNFNOEIINyz3fSEJ5NrVnNs&hl=uk&sa=X&ved=0ahUKEwjY9aHkk_bQAhVHC8AKHSnGDkU4ChDoAQhcMAg#v=onepage&q=corona%20lua%20socket%20server&f=false

--local port = 34952;
local port = 5555;
local address = "*"
local address = "10.10.10.79"

socket = require("socket");
server, err = socket.tcp();
if server == nil then
	print(err);
	os.exit();
end

server:setoption("reuseaddr", true);
server:setoption("tcp-nodelay", true);

res, err = server:bind(address, port);
if res == nil then
	print(err);
	os.exit();
end

res, err = server:listen(5);
if res == nil then
	print(err);
	os.exit();
end


Runtime:addEventListener("enterFrame",
	function()
		if client == nil then
			server:settimeout(0);

			client, _ = server:accept();
			if client ~= nil then
				client:setoption("tcp-nodelay", true);
				print(client:getpeername());
				btn13.getButton("connect"):setLabel( ""..client:getpeername() );
				btn13.enableButton("connect", true );
				clientConnected = true;
			end
		end
	end);

local host, port = server:getsockname();
print(""..host.." listening on port "..port)


